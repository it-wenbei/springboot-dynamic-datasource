package com.walking;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.walking.configuration.DynamicDataSourceConfig;
import com.walking.service.UserServiceV1;
import com.walking.service2.UserServiceV2;
import com.walking.service3.UserServiceV3;
import com.walking.service3.UserServiceV4;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
@SpringBootTest
//关掉数据源自动配置
@MapperScan(basePackages = "com.walking.dao")
@EnableAutoConfiguration(exclude = {DruidDataSourceAutoConfigure.class,
        DataSourceAutoConfiguration.class})
//导入我们自己的数据源配置
@Import({DynamicDataSourceConfig.class})
class AppTests {
    @Autowired
    private UserServiceV4 userServiceV4;
    @Autowired
    private UserServiceV3 userServiceV3;
    @Autowired
    private UserServiceV2 userServiceV2;
    @Autowired
    private UserServiceV1 userServiceV1;

    /**
     * 测试在方法上指定数据源SECOND
     */
    @Test
    public void test01(){
        userServiceV1.save_1();
        System.out.println("end");
    }
    /**
     * 方法上未指定，类上未指定，包未指定
     */
    @Test
    public void test02(){
        userServiceV1.save_2();
        System.out.println("end");
    }
    /**
     * 测试 方法上未指定，类上指定SECOND，则类中的都是相同的数据源
     */
    @Test
    void test03() {
        userServiceV2.save_1();
        userServiceV2.save_2();
        System.out.println("end");
    }

    /**
     * 测试 在方法和类上都未指定数据源，指定了包com.walking.service3都是SECOND数据源
     * 则包下所有类所有方法都是用相同数据源
     */
    @Test
    void test04() {
        userServiceV3.save_1();
        userServiceV3.save_2();

        userServiceV4.save();
        System.out.println("end");
    }

}
