package com.walking.db;

import java.lang.annotation.*;

/**
 * 多数据源注解
 * 指定要使用的数据源 可用于包，类型，方法
 *
 * @author walking
 * 公众号：编程大道
 */
@Target({ElementType.PACKAGE,ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CurDataSource {

    /**
     * name of DataSource
     * @return
     */
    DataSourceName value() default DataSourceName.FIRST;

}

