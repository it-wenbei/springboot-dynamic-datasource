package com.walking.db;

/**
 * @author walking
 * 公众号：编程大道
 */
public class DataSourceHolder {
    /**
     * ThreadLocal 用于提供线程局部变量，在多线程环境可以保证各个线程里的变量独立于其它线程里的变量。
     * 也就是说 ThreadLocal 可以为每个线程创建一个【单独的变量副本】，相当于线程的 private static 类型变量。
     */
    private static final ThreadLocal<DataSourceName> dataSourceName = new ThreadLocal<DataSourceName>();

    public static DataSourceName getDataSourceName(){
        DataSourceName dsName = dataSourceName.get();
        dataSourceName.remove();
        return dsName;
    }
    public static void setDataSourceName(DataSourceName dataSource){
        dataSourceName.set(dataSource);
    }
    public static void usePackageDatasourceKey(String pkName) {
        dataSourceName.set(DynamicDataSource.getPackageDatasource(pkName));
    }
}
