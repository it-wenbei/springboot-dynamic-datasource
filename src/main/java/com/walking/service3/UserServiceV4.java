package com.walking.service3;

import com.walking.mapper.UserMapper;
import com.walking.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceV4 {
    @Autowired
    private UserMapper userDAO;

    /**
     * 方法上未指定数据源，类上也指定，看{@link ../resources/spring-aop.xml}文件是否对该包配置了aop
     * 未指定com.walking.service3包，则使用默认的数据源first
     */
    public void save() {
        userDAO.save(UserUtil.createUser());
    }
}
