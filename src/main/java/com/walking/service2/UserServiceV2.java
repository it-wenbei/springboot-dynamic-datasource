package com.walking.service2;

import com.walking.mapper.UserMapper;
import com.walking.db.CurDataSource;
import com.walking.db.DataSourceName;
import com.walking.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@CurDataSource(DataSourceName.SECOND)
public class UserServiceV2 {
    @Autowired
    private UserMapper userDAO;

    /**
     * 方法上未指定数据源，类上指定
     */
    public void save_1() {
        userDAO.save(UserUtil.createUser());
    }

    public void save_2() {
        userDAO.save(UserUtil.createUser());
    }
}
