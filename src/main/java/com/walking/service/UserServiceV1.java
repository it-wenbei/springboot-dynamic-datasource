package com.walking.service;

import com.walking.mapper.UserMapper;
import com.walking.db.CurDataSource;
import com.walking.db.DataSourceName;
import com.walking.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceV1 {
    @Autowired
    private UserMapper userDAO;

    /**
     * 方法上指定数据源为SECOND
     */
    @CurDataSource(DataSourceName.SECOND)
    public void save_1() {
        userDAO.save(UserUtil.createUser());
    }

    /**
     * 方法上未指定数据源，则看类上指定的是什么，没有，再看包
     */
    public void save_2() {
        userDAO.save(UserUtil.createUser());
    }
}
