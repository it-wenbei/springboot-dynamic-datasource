package com.walking.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.Map;

public interface UserMapper {
    @Select("SELECT * from USER WHERE ID = #{id}")
    Map<String,Object> getUserById(int id);
    @Insert("insert into user(name,sex,age) values(#{name},#{sex},#{age})")
    void save(Map<String,Object> user);
}
