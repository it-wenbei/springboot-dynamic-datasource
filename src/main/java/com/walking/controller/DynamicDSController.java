package com.walking.controller;

import com.walking.service.UserServiceV1;
import com.walking.service2.UserServiceV2;
import com.walking.service3.UserServiceV3;
import com.walking.service3.UserServiceV4;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author walking
 * 公众号：编程大道
 */
@Slf4j
@RestController
public class DynamicDSController {
    @Autowired
    private UserServiceV4 userServiceV4;
    @Autowired
    private UserServiceV3 userServiceV3;
    @Autowired
    private UserServiceV2 userServiceV2;
    @Autowired
    private UserServiceV1 userServiceV1;

    /**
     * 测试在方法上指定数据源SECOND
     */
    @GetMapping("/test01")
    public String test01(){
        userServiceV1.save_1();
        log.info("end");
        return "OK";
    }
    /**
     * 方法上未指定，类上未指定，包未指定
     */
    @GetMapping("/test02")
    public String test02(){
        userServiceV1.save_2();
        log.info("end");
        return "OK";
    }
    /**
     * 测试 方法上未指定，类上指定SECOND，则类中的都是操作相同的数据源
     */
    @GetMapping("/test03")
    public String test03() {
        userServiceV2.save_1();
        userServiceV2.save_2();
        log.info("end");
        return "OK";
    }

    /**
     * 测试 在方法和类上都未指定数据源，指定了包com.walking.service3都是SECOND数据源
     * 则包下所有类所有方法都是用相同数据源
     */
    @GetMapping("/test04")
    public String test04() {
        userServiceV3.save_1();
        userServiceV3.save_2();

        userServiceV4.save();
        log.info("end");
        return "OK";
    }
}
